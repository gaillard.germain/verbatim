from flask import Flask
from verbatim_app.mod_main.views import mod_main

app = Flask(__name__)

app.config.from_object('config')

app.register_blueprint(mod_main)
