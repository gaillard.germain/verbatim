var language = $('html').attr('lang');


$(document).ready(function () {
    $('#word-input').val('');
    letterListener();
});


function toggleDropdown() {
  $('.dropdown-content').toggle("fast");
  $('#dropdown-button').toggleClass("fa-caret-square-down fa-caret-square-up");
};


$('#dropdown-button').on('click', function(event) {
  event.stopPropagation();
  event.preventDefault();
  toggleDropdown();
});


function update(response) {
  $('#total_score').text(response['total_score']);
  $('#total_ai_score').text(response['total_ai_score']);
  $('.letter-box').empty();
  for (const letter of response['pouch']) {
    console.log(letter);
    $letter = $('<div class="letter active"></div>');
    $letterText = $('<p class="letter-text"></p>').text(letter);
    $letterValue = $('<span class="letter-value"></span>').text(response['points'][letter]);
    $($letter).append($letterText);
    $($letter).append($letterValue);
    $('.letter-box').append($letter);
    letterListener()
  };
  $('#word-input').val('');
  if (language == 'en') {
    $('#submit-button').text("Skip");
  } else {
    $('#submit-button').text("Passer");
  };
  $('#word').text(response['word']);
  $('#score').text(response['score']);
  $('#ai_word').text(response['ai_word']);
  $('#ai_score').text(response['ai_score']);
  $('#warning-msg').text('');
  if (response['warning'] == 'no_solution') {
    if (language == 'en') {
      $('#warning-msg').text('No solution for this draw! Redealt all letters.');
    } else {
      $('#warning-msg').text('Pas de solution pour ce tirage! les lettres ont été redistribuées.');
    };
  } else if (response['warning'] == 'invalid_word') {
    if (language == 'en') {
      $('#warning-msg').text('Invalid word! Please try again.');
    } else {
      $('#warning-msg').text('Mot invalide! Réessayez.');
    };
  } else if (response['warning'] == 'skipped') {
    if (language == 'en') {
      $('#warning-msg').text('You lost ' + response['ai_score'] + ' points.');
    } else {
      $('#warning-msg').text('Vous perdez ' + response['ai_score'] + ' points.');
    };
  };
};


function sendToFlask(restart, lang) {
  $.ajax({
    url: '/',
    data: {
      lang: lang,
      restart: restart,
      word: $('#word-input').val()
    },
    type: 'POST'
    })
  .done(function(response) {
    console.log(response);
    if (restart == 'yes') {
      location.reload()
    } else {
      update(response);
    };
  });
};


$('#restart').on('click', function(event) {
  event.stopPropagation();
  event.preventDefault();
  toggleDropdown();
  sendToFlask('yes', language);
});


$('#english').on('click', function(event) {
  event.stopPropagation();
  event.preventDefault();
  toggleDropdown();
  sendToFlask('yes', 'en');
});


$('#french').on('click', function(event) {
  event.stopPropagation();
  event.preventDefault();
  toggleDropdown();
  sendToFlask('yes', 'fr');
});


$('#submit-button').on('click', function(event) {
  event.stopPropagation();
  event.preventDefault();
  sendToFlask('no', language);
});


$('#reset-button').on('click', function(event) {
  event.stopPropagation();
  event.preventDefault();
  $('.letter.inactive').toggleClass("inactive active");
  $('#word-input').val('');
  if (language === 'en') {
    $('#submit-button').text("Skip");
  } else if (language === 'fr') {
    $('#submit-button').text("Passer");
  }
});

function letterListener() {
  $('.letter.active').on('click', function(event) {
    event.stopPropagation();
    event.preventDefault();
    var input = $('#word-input')
    var letter = $('.letter-text', this).text()
    if (language === 'en') {
      if ($('#submit-button').text() != 'Submit') {
        $('#submit-button').text("Submit");
      }
    } else if (language === 'fr') {
      if ($('#submit-button').text() != 'Valider') {
        $('#submit-button').text("Valider");
      }
    }
    if ($(this).hasClass("active")) {
      input.val(input.val()+letter);
      $(this).toggleClass("active inactive")
    }
  });
};
